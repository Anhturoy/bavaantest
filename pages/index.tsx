import { Inter } from "next/font/google";
import { Dispatch, SetStateAction, useEffect, useState } from "react";
import axios from "axios";
import { BiSolidDownArrow, BiSolidUpArrow } from "react-icons/bi";
import { AiOutlineReload } from "react-icons/ai";
const inter = Inter({ subsets: ["latin"] });

interface DataICityProps {
  code: number;
  name: string;
  districts: {
    code: number;
    name: string;
    division_type: string;
  }[];
}

interface ValueFormProps {
  phone: string;
  email: string;
  city: string;
  districts: string;
}

enum TypeNotification {
  ERROR = "error",
  SUCCESS = "success",
}

export default function Home() {
  const [dataCity, setDataCity] = useState<DataICityProps[]>();
  const [ValueForm, setValueForm] = useState<ValueFormProps>({
    phone: "",
    email: "",
    city: "",
    districts: "",
  });
  const [cityselect, setCityselect] = useState<DataICityProps>();
  const [show, setShow] = useState(false);
  const [notification, setnotification] = useState({ type: "", mess: "" });

  const validateEmail = (email: string) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };

  function validatePhoneNumber(phoneNumber: string) {
    // Loại bỏ khoảng trắng và dấu gạch ngang (nếu có)
    const cleanedPhoneNumber = phoneNumber.replace(/\s|-/g, "");

    // Kiểm tra xem số điện thoại có chứa chữ số và có ít nhất 9 ký tự hay không
    return /^[0-9]{8,11}$/.test(cleanedPhoneNumber);
  }

  useEffect(() => {
    const fetchdata = async () => {
      const data = await axios("https://provinces.open-api.vn/api/?depth=2");
      setDataCity(data.data);
    };
    fetchdata();
  }, []);

  const handleChangeInput = (value: string, key: string) => {
    setValueForm({ ...ValueForm, [`${key}`]: value });
  };

  const handleSubmit = () => {
    for (const key in ValueForm) {
     let  keys  = key  as "phone" || "email" || "city" || "district"
      if (ValueForm.hasOwnProperty(key) && ValueForm[keys]  === "") {
        return setnotification({
          type: TypeNotification.ERROR,
          mess: `Vui lòng điền ${keys}`
        });
      }
    }
    if (!validatePhoneNumber(ValueForm.phone)) {
      return setnotification({
        type: TypeNotification.ERROR,
        mess: "Phone sai định dạng ",
      });
    }
    if (!validateEmail(ValueForm.email)) {
      return setnotification({
        type: TypeNotification.ERROR,
        mess: "Email sai định dạng ",
      });
    }
    setnotification({
      type: TypeNotification.SUCCESS,
      mess: "Thành Công",
    });
    setShow(true);
  };

  useEffect(() => {
    let time = 3;
    if (notification.type) {
      const timeRunning = setInterval(function () {
        time -= 1;
        if (time === 0) {
          setnotification({ type: "", mess: "" });
          clearInterval(timeRunning);
        }
        return time;
      }, 1000);
    }
  }, [notification]);

  console.log(notification.type);

  return (
    <div className="flex justify-center items-center h-screen text-black bg-[url('https://bavaan.com/wp-content/uploads/2022/10/Group-272.png')] bg-cover bg-center">
      {show ? (
        <div className="w-[410px]  flex flex-col items-start justify-between h-[600px] shadow-md rounded-lg border relative p-4 bg-white">
          <div
            className={`w-[200px] h-[36px] flex  items-center justify-center duration-300 text-white font-medium bg-black z-[999999999] ${
              notification.type !== "" ? "opacity-100" : "opacity-0"
            } absolute left-3  rounded-lg shadow-lg ${
              notification.type === TypeNotification.SUCCESS
                ? "bg-green-400"
                : "bg-red-400"
            }`}
          >
            {notification.mess}
          </div>
          <div className="w-full">
            <h1 className="text-3xl w-full text-center mb-10 font-bold text-[#299cfa] tracking-[3px]">
              Bavaan
            </h1>
            <div className=" pb-4 border-b mb-6 ">
              <p className="w-[80px] mr-2 text-lg ">Phone:</p>
              <p>{ValueForm.phone}</p>
            </div>
            <div className=" pb-4 border-b mb-6 ">
              <p className="w-[80px] mr-2 text-lg ">Email:</p>
              <p>{ValueForm.email}</p>
            </div>
            <div className=" pb-4 border-b mb-6 ">
              <p className="w-[80px] mr-2 text-lg ">City:</p>
              <p>{ValueForm.city}</p>
            </div>
            <div className=" pb-4 border-b mb-6 ">
              <p className="w-[80px] mr-2 text-lg ">District:</p>
              <p>{ValueForm.districts}</p>
            </div>

            <div className="flex items-center justify-center mt-10">
              <AiOutlineReload
                onClick={() => {
                  setValueForm({
                    phone: "",
                    email: "",
                    city: "",
                    districts: "",
                  });
                  setShow(false);
                }}
                className="text-[40px] cursor-pointer hover:scale-110 duration-300 hover:rotate-45"
              />
            </div>
          </div>
        </div>
      ) : (
        <div className="w-[410px] flex flex-col items-start justify-between h-[600px] shadow-md rounded-lg border relative p-4 bg-white">
          <div
            className={`w-[200px] h-[36px] flex items-center justify-center duration-300 text-white font-medium bg-black z-[999999999] ${
              notification.type !== "" ? "opacity-100" : "opacity-0"
            } absolute left-3  rounded-lg shadow-lg ${
              notification.type === TypeNotification.SUCCESS
                ? "bg-green-400"
                : "bg-red-400"
            }`}
          >
            {notification.mess}
          </div>

          <div className="w-full">
            <h1 className="text-3xl w-full text-center mb-10 font-bold text-[#299cfa] tracking-[3px] ">
              Bavaan
            </h1>
            <div className="flex items-center mb-10 ">
              <p className="w-[80px] mr-2 text-lg ">Phone:</p>
              <input
                placeholder="Your phone number"
                onChange={(e) => {
                  handleChangeInput(e.target.value, "phone");
                }}
                type="number"
                className="placeholder:italic px-4 w-[310px] py-2 border"
              />
            </div>
            <div className="flex items-center ">
              <p className="w-[80px] mr-2 text-lg ">Email:</p>
              <input
                placeholder="Your email address"
                onChange={(e) => {
                  handleChangeInput(e.target.value, "email");
                }}
                type="email"
                className="placeholder:italic px-4 w-[310px] py-2 border"
              />
            </div>

            <div>
              <Dropdown
                ValueForm={ValueForm}
                setValueForm={setValueForm}
                setCityselect={setCityselect}
                keycity={cityselect}
                data={dataCity}
                name="City:"
              />
            </div>
            <div>
              <Dropdown
                ValueForm={ValueForm}
                setValueForm={setValueForm}
                albe={true}
                keydistrict={ValueForm?.districts}
                datadistrict={cityselect?.districts}
                name="District:"
              />
            </div>
          </div>
          <div className="w-full flex justify-end">
            <button
              onClick={handleSubmit}
              className="px-4 py-2 mt-[50px] text-base font-medium shadow-md rounded-md text-white bg-red-400 hover:scale-110 duration-300 hover:bg-red-500 hover:text-white"
            >
              Submit
            </button>
          </div>
        </div>
      )}
    </div>
  );
}

const Dropdown = ({
  ValueForm,
  setValueForm,
  setCityselect,
  albe,
  keydistrict,
  keycity,
  data,
  name,
  datadistrict,
}: {
  ValueForm: ValueFormProps;
  setValueForm: Dispatch<SetStateAction<ValueFormProps>>;
  setCityselect?: Dispatch<SetStateAction<DataICityProps | undefined>>;
  albe?: boolean;
  datadistrict?:
    | {
        code: number;
        name: string;
        division_type: string;
      }[]
    | undefined;
  keydistrict?: string | undefined;
  keycity?: DataICityProps | undefined;
  data?: DataICityProps[] | undefined;
  name: string;
}) => {
  const [open, setOpen] = useState(false);
  const handleChange = (value: any) => {
    if (albe) {
      setValueForm({ ...ValueForm, districts: value.name });
      setOpen(false);
    } else {
      if (setCityselect && setValueForm) {
        setValueForm({ ...ValueForm, city: value.name });
        setCityselect(value);
        setOpen(false);
      }
    }
  };

  return (
    <div className="flex items-center mt-10 relative ">
      <p className="text-lg w-[80px] mr-2">{name}</p>
      <div
        onClick={() => {
          if (setCityselect) {
            setValueForm({ ...ValueForm, city: "", districts: "" });
            setCityselect(undefined);
          }
          setOpen(!open);
        }}
        className="w-[310px] px-4 py-2.5 border rounded-sm flex items-center cursor-pointer justify-between"
      >
        <div className="text-sm bg-white">
          {albe ? (
            <div> {keydistrict ? keydistrict : "__Quận huyện__"}</div>
          ) : (
            <div>{keycity ? keycity.name : "__Thành phố__"}</div>
          )}
        </div>
        <div className=" text-[8px]">
          <BiSolidUpArrow className="translate-y-[2px]" />
          <BiSolidDownArrow />
        </div>
      </div>
      {open && (datadistrict || data) ? (
        <div
          className={`w-[310px] p-1.5 border rounded-sm absolute top-[44px] left-[87px] max-h-[220px] overflow-y-auto	bg-white z-[99999]`}
        >
          {albe ? (
            <div>
              {datadistrict ? (
                <div>
                  {datadistrict.map((value, index) => {
                    return (
                      <div
                        onClick={() => {
                          handleChange(value);
                        }}
                        className="hover:!bg-gray-200 py-1  cursor-pointer duration-300 "
                        key={index}
                      >
                        {value.name}
                      </div>
                    );
                  })}
                </div>
              ) : null}
            </div>
          ) : (
            <div>
              {data ? (
                <div>
                  {data.map((value, index) => {
                    return (
                      <div
                        onClick={() => {
                          handleChange(value);
                        }}
                        className="hover:bg-gray-200 py-1 cursor-pointer duration-300 "
                        key={index}
                      >
                        {value.name}
                      </div>
                    );
                  })}
                </div>
              ) : null}
            </div>
          )}
        </div>
      ) : null}

      {/* {keycity} + {data} */}
    </div>
  );
};
